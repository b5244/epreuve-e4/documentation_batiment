+++
title = "Stations d'accueil"
+++

### Connectique USB-C :

Tous vos ordinateurs sont équipés de ports USB-C, ces ports permettent de connecter de multiples choses :
- Alimentation
- Périphériques USB
- Écrans externes
- Réseau

Ces connectiques sont présentes dans tout le bâtiment.

{{% notice info %}}
La norme USB-C ne prévoit de fournir que 100W d'énergie en alimentation, et la plupart des câbles USB-C ne fournissent que 65W.
Certains d'entre vous sont équipés de PCs Dell Precision qui demandent 130W d'alimentation. Vous pouvez les brancher sur les stations d'accueil Dell (câbles USB-C noirs) qui sont prévues pour.
Vous pouvez aussi les brancher sur les autres câbles USB-C mais vous risquez d'avoir un message d'information vous indiquant un problème de charge (vous pouvez l'ignorer).
{{% /notice %}}

![Connecteurs USB-C](/images/usbc/usbc.jpg)

### Câbles USB-C gris :

Ceux-là sont simplement des chargeurs, vous pouvez brancher votre PC dessus pour charger votre batterie. 

### Câbles USB-C bleus et noirs :

Ce sont les stations d'accueil. Nous avons fait en sorte que ce câble vous donne accès à tout ce qui est présent sur le poste de travail (pour les salles de réunion, c'est majoritairement vrai, vous reporter aux pages concernant les salles spécifiquement).

Typiquement, à votre poste de travail, seront accessibles grâce à l'USB-C :
- Les deux écrans
- Ce que vous branchez sur les ports USB à l'arrière des écrans 27"
- Ce que vous branchez sur les câbles de charge micro-USB et USB-C présents à l'arrière de vos écrans (vous pouvez y brancher votre téléphone par exemple)
- Le réseau
- Le clavier
- L'alimentation

{{% notice warning %}}
Nous avons rencontré quelques problèmes concernant l'affichage, la sortie de veille et les stations d'accueil. Si vous avez les écrans qui se bloquent ou ne se rallument pas, contactez le système interne (Bruno, Pierre-Emmanuel ou Lukas).
{{% /notice %}}
