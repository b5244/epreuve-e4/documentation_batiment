+++
title = "Climatisation"
+++

### Principes généraux

La climatisation est présente dans tout le bâtiment. Un circuit ou bloc de climatisation individuel est présent dans chaque pièce, sauf dans l'open space où il y en a trois.

Une programmation s'occupe de régler la température et le mode de fonctionnement des climatisations.
En plus de cela il y a dans chaque pièce un panneau de contrôle de la clim.

### Programmation générale :

Il y a un mode été et un mode hiver.

En été, la température est réglée sur 25° C et le mode sur refroidissement.

En hiver, la température est réglée sur 21° C et le mode sur chauffage.

Dans les salles où l'on ne travaille pas toute la journée, type salles de réunion, la température est réglée sur une température de réserve type Eco, n'hésitez pas à l'ajuster en entrant dans la salle.

### Panneau de contrôle individuel :

{{% notice warning %}}
Quoi que vous fassiez sur les panneaux de contrôle individuels, la programmation reprendra la main dans la nuit et vos réglages seront effacés le lendemain. **C'est le fonctionnement normal et voulu.**
{{% /notice %}}

Vous pouvez allumer et éteindre les climatisations et ajuster leur température, mais vous ne pouvez pas changer le mode de fonctionnement (passer de froid à chauffage).

Pour allumer et éteindre la climatisation, appuyez sur le bouton on/off.

![Allumage et extinction de la climatisation](/images/climatisation/controle_on_off.jpg)

Pour ajuster la température, appuyez plusieurs fois sur + et - puis sur Ok.

![Réglage température](/images/climatisation/controle_temp.jpg)