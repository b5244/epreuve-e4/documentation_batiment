+++
title = "Contrôle d'accès"
+++

{{% notice warning %}}
Attention à lire bien attentivement cette page, une erreur de manipulation peut vite déclencher une intervention de la société de surveillance, intervention qui nous sera facturée.
Au moindre problème, vous pouvez contacter l'une de ces trois personnes : **Matthieu PIAUMIER** (00 00 00 00 00), **Pierre CASTELNAU** (00 00 00 00 00) ou **Pierre-emmanuel VIVER** (00 00 00 00 00)
{{% /notice %}}

### Principe de fonctionnement de l'alarme :

Le bâtiment est mis sous surveillance par une alarme. Seuls les intérieurs sont soumis à cette surveillance, il n'y a pas d'alarme sur les extérieurs.

Des platines à badge permettent de l'activer ou de la désactiver. Les voyants indiquent son état. Des détecteurs de mouvements dans les couloirs et les espaces communs vérifient la présence ou non de personne.

Elle peut être **activée** ou pas :

![Image des voyants de l'alarme](/images/alarme/alarme_voyants_entree_employes.jpg)

Si elle est activée et que les détecteurs de mouvements captent quelqu'un à l'intérieur du bâtiment, alors elle se **déclenche**. La sonnerie retentit et une équipe d'agents de sécurité se déplace pour vérifier s'il y a une intrusion.

### Plages horaires de fonctionnement de l'alarme :

L'alarme est programmée pour pouvoir être désactivée entre 8 h et 21 h 30 du lundi au vendredi. Si vous entrez dans le bâtiment le soir ou le weekend, Matthieu, Pierre et Pierre-emmanuel seront prévenus. Rien de grave, vous avez le droit, pensez simplement à leur signaler votre présence pour leur éviter des frayeurs inutiles !
- **Matthieu PIAUMIER** (06 22 09 39 97)
- **Pierre Castelnau** (06 65 26 93 73)
- **Pierre-emmanuel VIVER** (06 33 90 89 64)

### Je veux entrer dans le bâtiment, passer le portail ou entrer dans le local à vélos :

Les portes du bâtiment sont fermées par des serrures magnétiques. Pour entrer dans le bâtiment, il vous suffit de passer votre badge devant la platine prévue à cet effet.

![Entrée dans le bâtiment](/images/alarme/alarme_badgeuse_en_action_entree_employes.jpg)

Pour sortir, vous trouverez à proximité des portes des interrupteurs ouvre-portes (Pour le portail, il faut passer le badge sur la platine à l'entrée du tunnel).

![Ouvre porte intérieur](/images/alarme/alarme_entree_employes_interieur.jpg)

Rien de spécial ici, vous avez certainement connu ce fonctionnement en habitant dans un immeuble.

### Je veux activer ou désactiver l'alarme

Le matin, la première personne qui entre désactive l'alarme. En passant simplement son badge pour entrer, l'alarme passe au vert et est désactivée.

Le soir, la dernière personne qui sort **DOIT** activer l'alarme. Pour cela, il faut sortir, attendre que la porte soit bien fermée, passer le badge une fois, puis une deuxième fois.

L'alarme passe au rouge, elle est activée, vous pouvez y aller !

### J'arrive avant 8h, je reste après 21h30 ou je viens le weekend :

Vous êtes en dehors des plages normales de désactivation de l'alarme. Elle tentera de se réactiver toutes les 30 minutes. Vous serez prévenus par la sonnerie d'alerte qui retentira quelques secondes. Il faut aller passer le badge sur l'une des platines de relance afin de repousser l'activation de 30 minutes supplémentaires.

Les platines de relance se trouvent :
* dans l'open space,
* dans le couloir devant la salle serveur (le lecteur rond),
* en haut de l'escalier de l'entrée à côté de la porte de la terrasse,
* à l'exterieur au niveau de l'entrée du personnel.


![Platine de relance Open space](/images/alarme/alarme_open_space.jpg) ![Platine couloir salle serveur](/images/alarme/alarme_salle_serveur.jpg) ![Platine étage](/images/alarme/alarme_badgeuse_relance_etage.jpg)


Lors de vos venus les w-e ou en soirée prévenez Matthieu, Pierre ou Pierre-emmanuel de votre arrviée, ça évitera du stress !
