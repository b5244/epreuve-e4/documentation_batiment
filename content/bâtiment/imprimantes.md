+++
title = "Imprimantes"
+++

{{% notice warning %}}
En raison d'un bug du serveur d'impression Windows, l'utilisation de nos imprimantes se fait en mode dégradé. Nous sommes en attente d'un correctif de la part de Microsoft et mettrons cette documentation à jour dès que le mode normal sera rétabli. Pour l'instant n'est documenté que ce qui marche.
{{% /notice %}}

### Emplacement des imprimantes :

La Xerox Workcentre 6515 est pour l'instant située dans la salle de reprographie dans le couloir de direction.

La xerox C505 est située sur la coursive de l'Open Space (elle ne fonctionne, pour l'instant, que pour l'impression depuis les PCs Linux).

### Procédure pour imprimer

{{< tabs groupId="OS" >}}
{{% tab name="Windows" %}}

#### Impression classique

Pour imprimer mon fichier, je sélectionne l'imprimante Workcentre 6515 PS. (Située en salle de reprographie)

Je lance l'impression.

{{% /tab %}}
{{% tab name="Ubuntu" %}}

#### Impression classique

Pour imprimer mon fichier, je sélectionne l'imprimante Xerox Tucano. (Qui est la 6515 située en salle de reprographie)

Je lance l'impression.

#### Impression via le serveur Papercut

1. Sur mon poste, je lance l'application "Xerox"

![Lancement de Papercut](/images/imprimantes/lancement_papercut.png?width=1000px)

2. je vérifie que mon solde d'impression s'affiche bien sur l'un de mes écrans.

![Interface de Papercut](/images/imprimantes/papercut_interface.png)

3. Je lance mon impression en sélectionnant Xerox-global-2

![Sélection de l'imprimante](/images/imprimantes/dialogue_xerox_global_2.png?width=1000px)

4. Une fenêtre apparaît, me demandant de m'authentifier avec mes identifiants LDAP.

![Authentification LDAP](/images/imprimantes/connexion_ldap.png)

5. Après un message de confirmation, mon impression est présente sur le serveur.

![Message de confirmation](/images/imprimantes/message_normal.png)

6. Je peux consulter ma file d'attente en cliquant sur **détails** puis en m'authentifiant sur l'interface web.

![Interface de Papercut](/images/imprimantes/papercut_interface.png)

![Authentification interface web](/images/imprimantes/connexion_webinterface.png?width=1000px)

![File d'attente](/images/imprimantes/travaux_attente.png?width=1000px)
{{% /tab %}}
{{< /tabs >}}

### Faire sortir mon impression de l'imprimante :

{{< tabs groupId="methode_impression" >}}
{{% tab name="Impression classique" %}}
Je n'ai rien à faire après les étapes précédentes, mon impression va sortir toute seule.
{{% /tab %}}
{{% tab name="Serveur Papercut sans se déplacer" %}}
Dans l'interface web, je clique sur imprimer :

![Impression](/images/imprimantes/travaux_attente.png?width=1000px)

Puis je sélectionne l'imprimante :

![Selection de l'imprimante](/images/imprimantes/choix_imprimante.png?width=1000px)

Enfin je libère mes impressions (tout libérer) :

![Tout libérer](/images/imprimantes/travaux_attente.png?width=1000px)

{{% /tab %}}
{{% tab name="Serveur Papercut en se déplaçant" %}}
Je vais jusqu'à l'imprimante, puis m'authentifie grâce à mon numéro de téléphone Xivo.

![Page de connexion](/images/imprimantes/C505_connexion.jpg)

{{% /tab %}}