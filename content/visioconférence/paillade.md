+++
title = "Paillade"
+++

### Equipements :
Cette salle est équipée de :
- Deux stations d'accueil DELL
- Une télé connectée Viewsonic 55"
- Une webcam/micro/haut parleur Yealink UVC40
- Une pieuvre micro/haut parleur Yealink CP960
- Deux câbles de charge USB-C gris
- Une station de charge par induction pour les téléphones portables
- Un câble de charge Micro-USB noir et gris


{{% notice warning %}}
Pour utiliser les stations d'accueil de cette pièce, il y a un switch sous la table pour faire la connexion de l'écran sur la bonne station d'accueil. Il peut être actionné par sa télécommande ou par un bouton sur son capot.
{{% /notice %}}


### Je veux faire une visioconférence

{{< tabs groupId="os" >}}
{{% tab name="Windows"%}}
1. Je me branche sur la station d'accueil Dell en face de l'écran (USB-C de couleur noire)
2. J'allume la télé
3. Je pense à sélectionner le bon canal sur le switch HDMI
4. Je paramètre le son de mon PC pour utiliser l'UVC40 en entrée et en sortie

![paramètres son sur Windows](/images/paillade/beaux_arts_windows_choix_son.png)

4. Je lance la visio sur mon PC portable et je sélectionne la bonne caméra (UVC40)

![sélection de la bonne caméra](/images/paillade/beaux_arts_image_uvc40.png)
{{% /tab %}}
{{% tab name="Ubuntu" %}}
1. Je me branche sur la station d'accueil Dell en face de l'écran (USB-C de couleur noire)
2. J'allume la télé
3. Je paramètre le son de mon PC pour utiliser l'UVC40 en entrée et en sortie

![paramètres son sur Ubuntu](/images/paillade/linux_beaux_arts_son_uvc40.png)

4. Je lance la visio sur mon PC portable et je sélectionne la bonne caméra (UVC40)

![sélection de la bonne caméra](/images/paillade/beaux_arts_image_uvc40.png)
{{% /tab %}}
{{% tab name="MacOs" %}}
1. Je me branche sur la station d'accueil Dell en face de l'écran (USB-C de couleur noire)
2. J'allume la télé
3. Je paramètre le son de mon mac pour utiliser l'UVC40 en entrée et en sortie
4. Je lance la visio en sélectionnant l'UVC40

nota : Nous rencontrons pour le moment des problèmes de compatibilité avec MacOs, le comportement n'est pas le même d'un poste à l'autre. Si vous avez du mal à utiliser l'écran et/ou l'UVC40, nous avons un poste sous Ubuntu à vous prêter le temps de votre réunion

{{% /tab %}}
{{< /tabs >}}


### Je veux partager mon écran avec fil

1. Je me branche sur l'une des deux stations d'accueil Dell (USB-C de couleur noire)
2. J'allume la télé.
3. Je pense à sélectionner le bon canal sur le switch HDMI. 
   - **Choix 1 :** En face de l'écran
   - **Choix 2 :** Debout à côté de l'écran

![telecommande switch hdmi](/images/paillade/paillade_telecommande_switch.jpg)


### Je veux partager mon écran sans fil

{{< tabs groupId="os" >}}
{{% tab name="Windows" %}}
1. J'allume la télé
2. Sur la télécommande j'appuie sur le bouton "Maison"

![Télécommande des viewsonics](/images/paillade/beaux_arts_telecommande.png)

4. Sur mon PC j'ouvre le navigateur basé sur chromium (Google Chrome, Brave, Edge par exemple), je clique sur le menu des options (les trois tirets)
5. Dans le menu déroulant je clique sur "caster"

![On clique sur caster](/images/paillade/linux_beaux_arts_cast_menu.png)

6. Je sélectionne la source (onglet, fenêtre ou l'un de mes écrans)

![On sélectionne la source](/images/paillade/linux_beaux_arts_cast_menu_source.png)

7. Je sélectionne la télé "CDE5520_LG2-199" (Oui, le nom sera changé)

![On sélectionne le bon écran](/images/paillade/linux_paillade_cast_menu.png)

{{% /tab %}}
{{% tab name="Ubuntu" %}}
1. J'allume la télé
2. Sur la télécommande j'appuie sur le bouton "Maison"

![Télécommande des viewsonics](/images/paillade/beaux_arts_telecommande.png)

4. Sur mon PC j'ouvre le navigateur basé sur chromium (Google Chrome, Brave, Edge par exemple), je clique sur le menu des options (les trois tirets)
5. Dans le menu déroulant je clique sur "caster"

![On clique sur caster](/images/paillade/linux_beaux_arts_cast_menu.png)

6. Je sélectionne la source (onglet, fenêtre ou l'un de mes écrans)

![On sélectionne la source](/images/paillade/linux_beaux_arts_cast_menu_source.png)

7. Je sélectionne la télé "CDE5520_LG2-199" (Oui, le nom sera changé)

![On sélectionne le bon écran](/images/paillade/linux_paillade_cast_menu.png)

{{% /tab %}}
{{% tab name="MacOs" %}}
1. J'allume la télé
2. Sur la télécommande j'appuie sur le bouton "Maison"

![Télécommande des viewsonics](/images/beaux_arts/beaux_arts_telecommande.png)

3. Avec la télécommande je sélectionne l'application VCastReceiver

![Je sélectionne l'application VCastReceiver](/images/paillade/paillade_tele_vcast_accueil.jpg)

4. Je me connecte en airplay grâce aux identifiants qui sont affichés.

![Je me connecte en Airplay](/images/paillade/paillade_tele_vcast_appli.jpg)

   {{% /tab %}}
   {{< /tabs >}}

### Je veux faire un appel téléphonique

{{% notice info %}}
Cette salle est équipée de deux micros déportés appairés à la pieuvre, vous pouvez les disposer sur la table en pensant bien à les remettre sur leur base à la fin (dans le bon sens).
{{% /notice %}}

![micros pieuvre](/images/paillade/paillade_pieuvre_micros.jpg)

![micros pieuvre déportés](/images/paillade/paillade_pieuvre_micros_deportes.jpg)



#### Avec mon téléphone portable

1. J'appaire mon téléphone en bluetooth avec la pieuvre
2. Je téléphone

#### Avec la pieuvre directement

{{% notice info %}}
Le numéro xivo de la pieuvre apparaît sur sa page d'accueil
{{% /notice %}}

![Page d'accueil de la pieuvre CP960](/images/paillade/paillade_pieuvre_accueil.jpg)

1. Je compose le numéro

![Page pour composer les numéros de la pieuvre CP960](/images/paillade/paillade_pieuvre_composer.jpg)
