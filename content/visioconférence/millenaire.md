+++
title = "Millénaire"
+++

{{% notice warning %}}
Pour allumer les équipements, bien respecter l'ordre d'allumage : 
les deux télés (télécommande Viewsonic) -> la barre de son (télécommande Sony) -> Système de visio (télécommande Yealink).
{{% /notice %}}

![Ordre d'allumage des appareils](/images/millenaire/millenaire_telecommandes.png)

### Liens pour les réunions :

Le lien pour la salle permanente sera posté tous les matins sur le canal public de Mattermost. Il en sera de même les vendredi  pour le lien de la réunion petit déjeuner/revue de comptes.

{{< tabs groupId="reunion" >}}
{{% tab name="Petit déjeuner et revue de comptes" %}}
Lancez l'appel dans les contacts de la tablette, et partagez ce lien aux intervenants : https://meet.starleaf.com/4112213916
{{% /tab %}}
{{% tab name="Salle permanente" %}}
Lancez l'appel dans les contacts de la tablette, et partagez ce lien aux intervenants : https://meet.starleaf.com/4128946116
{{% /tab %}}
{{< /tabs >}}

### Equipements

Cette salle est équipée de :
- Un système de visio autonome VC800 avec sa tablette de contrôle et ses micros
- Un système de partage d'écran WPP20
- Deux télés Viewsonic 55" (partagent la même télécommande)
- Deux stations d'accueil DELL (câbles noirs) (Accessibles dans la goulotte blanche au centre de la table)
- Deux câbles de charge USB-C de couleur grise (Accessibles dans la goulotte blanche au centre de la table)

### Je veux faire une présentation à des personnes présentes dans la salle

{{% notice info %}}
Le système est multiprésentateur, c'est la dernière personne connectée qui est projetée, et ça revient à l'avant dernière lorsque la dernière se déconnecte.
{{% /notice %}}

#### Pour tout le monde :

- Sur la tablette de contrôle je clique sur Présentation

![Présentation sur la tablette de contrôle](/images/millenaire/millenaire_vc800_accueil_presentation.jpg)

- Je me branche sur l'une des deux stations d'accueil Dell (câbles USB-C de couleur noire)
- L'affichage apparaît sur les deux écrans au mur
- Il apparaît aussi sur la tablette, me donnant accès à divers outils (dessin, laser, contrôle de l'écran en tactile, ...)

#### Mode supplémentaires spécifiques :

{{< tabs groupId="os" >}}
{{% tab name="Windows"%}}
- Je branche le dongle WPP20 sur un port USB de mon PC

![WPP20](/images/millenaire/millenaire_wpp20.jpg)

- Sur la tablette de contrôle, je clique sur Présentation.

![Présentation sur la tablette de contrôle](/images/millenaire/millenaire_vc800_accueil_presentation.jpg)

- J'appuie sur le bouton du dongle, mon écran s'entoure de rouge -> je suis en train de partager

![Partage Windows WPP20](/images/millenaire/bord_rouge_windows_WPP20.jpg)

- L'affichage apparaît sur les deux écrans au mur
- Il apparaît aussi sur la tablette, me donnant accès à divers outils (dessin, laser, contrôle de l'écran en tactile, ...)
{{% /tab %}}
{{% tab name="Ubuntu" %}}
Rien de plus
{{% /tab %}}
{{% tab name="MacOs" %}}
- Sur la tablette de contrôle je clique sur Présentation

![Présentation sur la tablette de contrôle](/images/millenaire/millenaire_vc800_accueil_presentation.jpg)

- Je me connecte via AirPlay grâce aux identifiants affichés.

![Airplay sur VC800](/images/millenaire/millenaire_vc800_presentation.jpg)
{{% /tab %}}
{{< /tabs >}}   

### Je veux faire une visio seulement

{{< tabs groupId="cas_millenaire" >}}
{{% tab name="Les participants rejoignent la salle Libriciel" %}}
- Sur la tablette je clique sur Composer

![Présentation sur la tablette de contrôle](/images/millenaire/millenaire_vc800_accueil_composer.jpg)

- Je clique sur contacts puis sur la salle "Revue de comptes" ou "Salle permanente LIBRICIEL" selon le besoin

![composer VC800](/images/millenaire/millenaire_vc800_composer_contacts.jpg)

![Contacts VC800](/images/millenaire/millenaire_vc800_contacts.jpg)

- Je partage le lien de la réunion aux participants :
  - Revue de comptes : https://meet.starleaf.com/4112213916
  - Salle permanente : https://meet.starleaf.com/4128946116
{{% /tab %}}
{{% tab name ="Je rejoins une salle externe" %}}
1. Je demande bien en amont de la réunion l'indentifiant h.323 à mon interlocuteur (de la forme ip#identifiant, par exemple 255.255.255.255#123456789)
2. Sur la tablette je clique sur composer :

![VC800 Composer](/images/millenaire/millenaire_vc800_accueil_composer.jpg)

3. Je compose l'identifiant donné par le correspondant en pensant à sélectionner "H323" au lieu de "automatique" en bas :

![VC800 h.323](/images/millenaire/millenaire_vc800_composer_numero.jpg)

{{% /tab %}}
{{< /tabs >}}

### Je veux faire une visio et partager mon écran

{{< tabs groupId="os" >}}
{{% tab name="Windows"%}}
- Après avoir suivi les étapes pour me connecter à la visio, je branche le dongle WPP20 sur mon PC

![WPP20](/images/millenaire/millenaire_wpp20.jpg)

- Sur la tablette de contrôle, je clique sur Présentation.

![Présentation sur la tablette de contrôle](/images/millenaire/millenaire_vc800_appel.jpg)

- J'appuie sur le bouton du dongle, mon écran s'entoure de rouge -> je suis en train de partager

![Partage Windows WPP20](/images/millenaire/bord_rouge_windows_WPP20.jpg)

{{% /tab %}}
{{% tab name="Ubuntu" %}}
- Après avoir suivi les étapes pour me connecter à la visio, je me branche à l'une des deux stations d'accueil en USB-C

- Sur la tablette de contrôle je clique sur Présentation

![Présentation sur la tablette de contrôle](/images/millenaire/millenaire_vc800_appel.jpg)

{{% /tab %}}
{{% tab name="MacOs" %}}
- Je suis les étapes pour me connecter à la visio
- Sur la tablette de contrôle je clique sur Présentation

![Présentation sur la tablette de contrôle](/images/millenaire/millenaire_vc800_appel.jpg)

- Je me connecte via AirPlay grâce aux identifiants affichés.

![Airplay sur VC800](/images/millenaire/millenaire_vc800_presentation.jpg)
{{% /tab %}}
{{< /tabs >}}   
