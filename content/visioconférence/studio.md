+++
title = "Studio"
+++

### Equipements :
Cette salle est équipée de :
- Un matériel d'enregistrement audio pour deux voix
- Une webcam usb Lenovo

{{% notice warning %}}
Pour allumer correctement le studio, allumer d'abord l'interface audio Focusrite (bouton on/off à l'arrière droite du chassis) puis **ensuite** les deux enceintes Yamaha (boutons ON/OFF à l'arrière gauche de chaque enceinte)
{{% /notice %}}

### Je veux faire une visio ou un webinaire

1. Je me branche à la station d'accueil Kensington (câble USB-C bleu)
2. Si je suis sous Windows ou MacOs, j'installe les drivers/logiciel de contrôle :
    - Windows : https://fael-downloads-prod.focusrite.com/customer/prod/s3fs-public/downloads/Focusrite%20Control%20-%203.6.0.1822.exe
    - MacOs : https://fael-downloads-prod.focusrite.com/customer/prod/s3fs-public/downloads/Focusrite%20Control%20-%203.7.4.1943.dmg
3. Je peux sélectionner dans les préférences audio du système les entrées et sorties audio de la Focusrite

{{% notice info %}}
Les deux micros sont branchés sur les entrées 1 pour celui de gauche et 2 pour celui de droite, les enceintes sont branchées sur la sortie 1 pour celle de gauche, et 2 pour celle de droite
{{% /notice %}}

4. Pour la webcam, il me suffit de la sélectionner au moment de partager ma caméra
