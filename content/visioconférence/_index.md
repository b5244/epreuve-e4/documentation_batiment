+++
menuTitle = "  Visioconférence"
title = "Visioconférence"
pre = "<i class='fas fa-video'></i>"
alwaysopen = true
+++

Voici la liste des salles équipées en matériel de visioconférence :

{{% children %}}

