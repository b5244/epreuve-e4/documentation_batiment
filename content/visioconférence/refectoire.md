+++
title = "Réfectoire"
+++

### Lien pour les réunions :

Le lien pour la salle permanente sera posté tous les matins sur le canal public de Mattermost. Il en sera de même les vendredi  pour le lien de la réunion petit déjeuner/revue de comptes.

{{< tabs groupId="reunion" >}}
{{% tab name="Revue de comptes" %}}
Lancez l'appel dans les contacts de la tablette, et partagez ce lien aux intervenants : https://meet.starleaf.com/4112213916
{{% /tab %}}
{{% tab name="Salle permanente" %}}
Lancez l'appel dans les contacts de la tablette, et partagez ce lien aux intervenants : https://meet.starleaf.com/4128946116
{{% /tab %}}
{{< /tabs >}}

### Equipements :
Cette salle est équipée de :
- Un système de visio autonome Poly X50
- Une télé Viewsonic 55"

### Je veux rejoindre l'une des salles de visioconférence de Libriciel

1. J'allume la télévision
2. Sur la tablette de contrôle je clique sur l'un des deux boutons verts et je partage le bon lien à mes interlocuteurs :
    - Petit déjeuner et revue de comptes : https://meet.starleaf.com/4112213916
    - Salle permanente Libriciel : https://meet.starleaf.com/4128946116

### Je veux faire un appel :

{{< tabs id="appel poly" >}}
{{% tab name="Vers POLY X50" %}}
- Le numéro Xivo de la Poly X50 est le 777
{{% /tab %}}
{{% tab name="Depuis la POLY X50" %}}
1. Je clique sur composer
2. Je fais un numéro de téléphone
{{% /tab %}}
{{< /tabs >}}

### Une personne externe (client, prestataire, partenaire...) veux ajouter le réfectoire à sa réunion Starleaf :

{{% notice note %}}
La personne externe possède un compte Starleaf et souhaite planifier une réunion.
Le contact chez Libriciel indique à son interlocuteur l'adresse mail : pierre-emmanuel.viver@libriciel.call.sl
{{% /notice %}}

Le jour de la réunion, le rendez vous apparaît sur la tablette :
1. J’allume la télé
2. Sur la tablette je clique sur le bouton vert qui est apparu
