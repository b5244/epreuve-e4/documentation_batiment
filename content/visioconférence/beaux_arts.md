+++
title = "Beaux arts"
+++


### Equipements :
Cette salle est équipée de :
- Une station d'accueil Kensington
- Une télé connectée Viewsonic 55"
- Une webcam/micro/haut parleur Yealink UVC40
- Une pieuvre micro/haut parleur Yealing CP960
- Un câble de charge USB-C gris
- Un câble de charge Micro-USB noir et gris


### Je veux faire une visioconférence 

{{< tabs groupId="os" >}}
{{% tab name="Windows"%}}
1. Je me branche sur la station d'accueil Kensington (USB-C de couleur bleue)
2. J'allume la télé
3. Je paramètre le son de mon PC pour utiliser l'UVC40 en entrée et en sortie

![paramètres son sur Ubuntu](/images/beaux_arts/beaux_arts_windows_choix_son.png)

4. Je lance la visio sur mon PC portable et je sélectionne la bonne caméra (UVC40)

![sélection de la bonne caméra](/images/beaux_arts/beaux_arts_image_uvc40.png)
{{% /tab %}}
{{% tab name="Ubuntu" %}}
1. Je me branche sur la station d'accueil Kensington (USB-C de couleur bleue)
2. J'allume la télé
3. Je paramètre le son de mon PC pour utiliser l'UVC40 en entrée et en sortie

![paramètres son sur Ubuntu](/images/beaux_arts/linux_beaux_arts_son_uvc40.png)

4. Je lance la visio sur mon PC portable et je sélectionne la bonne caméra (UVC40)

![sélection de la bonne caméra](/images/beaux_arts/beaux_arts_image_uvc40.png)
{{% /tab %}}
{{% tab name="MacOs" %}}
1. Je me branche sur la station d'accueil Kensington (USB-C de couleur bleue)
2. J'allume la télé
3. Je paramètre le son de mon mac pour utiliser l'UVC40 en entrée et en sortie
4. Je lance la visio en sélectionnant l'UVC40

nota : Nous rencontrons pour le moment des problèmes de compatibilité avec MacOs, le comportement n'est pas le même d'un poste à l'autre. Si vous avez du mal à utiliser l'écran et/ou l'UVC40, nous avons un poste sous Ubuntu à vous prêter le temps de votre réunion

{{% /tab %}}
{{< /tabs >}}

### Je veux partager mon écran sans fil

{{< tabs groupId="os" >}}
{{% tab name="Windows" %}}
1. J'allume la télé
2. Sur la télécommande j'appuie sur le bouton "Maison"

![Télécommande des viewsonics](/images/beaux_arts/beaux_arts_telecommande.png)

3. Sur mon PC j'ouvre un navigateur basé sur chromium (Google Chrome, Brave, Edge par exemple), je clique sur le menu des options (les trois tirets)
4. Dans le menu déroulant je clique sur "caster"

![On clique sur caster](/images/beaux_arts/linux_beaux_arts_cast_menu.png)

5. Je sélectionne la source (onglet, fenêtre ou l'un de mes écrans)

![On sélectionne la source](/images/beaux_arts/linux_beaux_arts_cast_menu_source.png)

6. Je sélectionne la télé "TV-BEAUX-ARTS"

![On sélectionne le bon écran](/images/beaux_arts/linux_beaux_arts_cast_menu2.png)

{{% /tab %}}
{{% tab name="Ubuntu" %}}
1. J'allume la télé
2. Sur la télécommande j'appuie sur le bouton "Maison"

![Télécommande des viewsonics](/images/beaux_arts/beaux_arts_telecommande.png)

3. Sur mon PC j'ouvre le navigateur basé sur chromium (Google Chrome, Brave, Edge par exemple), je clique sur le menu des options (les trois tirets)
4. Dans le menu déroulant je clique sur "caster"

![On clique sur caster](/images/beaux_arts/linux_beaux_arts_cast_menu.png)

5. Je sélectionne la source (onglet, fenêtre ou l'un de mes écrans)

![On sélectionne la source](/images/beaux_arts/linux_beaux_arts_cast_menu_source.png)

6. Je sélectionne la télé "TV-BEAUX-ARTS"

![On sélectionne le bon écran](/images/beaux_arts/linux_beaux_arts_cast_menu2.png)

{{% /tab %}}
{{% tab name="MacOs" %}}
1. J'allume la télé
2. Sur la télécommande j'appuie sur le bouton "Maison"

![Télécommande des viewsonics](/images/beaux_arts/beaux_arts_telecommande.png)

3. Avec la télécommande je sélectionne l'application VCastReceiver

![On sélectionne l'appli VCastReceiver](/images/beaux_arts/beaux_arts_tele_vcastreceiver_accueil.jpg)

4. Je me connecte en airplay grâce aux identifiants qui sont affichés.

![On utilise les identifiants affichés](/images/beaux_arts/beaux_arts_tele_vcastreceiver_appli.jpg)

{{% /tab %}}
{{< /tabs >}}

### Je veux faire un appel téléphonique

#### Avec Xivo sur mon PC

{{< tabs groupId="os" >}}
{{% tab name="Windows" %}}
-Je me branche sur la station d'accueil Kensington (USB-C de couleur bleue)
- Je paramètre le son de mon PC pour utiliser la CP960 ou la UVC40 en entrée et en sortie

![On sélectionne la CP960 dans les paramètres sons](/images/beaux_arts/beaux_arts_windows_choix_son.png)

- Je téléphone avec Xivo
{{% /tab %}}
{{% tab name="Ubuntu" %}}
- Je me branche sur la station d'accueil Kensington (USB-C de couleur bleue)
- Je paramètre le son de mon PC pour utiliser la CP960 ou la UVC40 en entrée et en sortie

![On sélectionne la CP960 dans les paramètres sons](/images/beaux_arts/linux_beaux_arts_son_cp960.png)

- Je téléphone avec Xivo
{{% /tab %}} 
{{% tab name="MacOs" %}}
- Je me branche sur la station d'accueil Kensington (USB-C de couleur bleue)
- Je paramètre le son de mon PC pour utiliser la CP960 ou la UVC40 en entrée et en sortie
- Je téléphone avec Xivo
{{% /tab %}}
{{< /tabs >}}
#### Avec mon téléphone portable

1. J'appaire mon téléphone en bluetooth avec la pieuvre
2. Je téléphone

#### Avec la pieuvre directement

{{% notice info %}}
Je peux appeler la pieuvre grâce à son numéro Xivo qui apparaît sur la page d'accueil
{{% /notice %}}

![Page d'accueil de la pieuvre CP960](/images/beaux_arts/beaux_arts_pieuvre_accueil.jpg)

1. Je compose le numéro

![Page pour composer les numéros de la pieuvre CP960](/images/beaux_arts/beaux_arts_pieuvre_composer.jpg)
