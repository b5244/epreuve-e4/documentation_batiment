+++
title = "Arceaux"
+++

### Equipements :

Cette salle est équipée de :
- Une station d'accueil Dell
- Deux écrans 24"
- Une webcam et une pieuvre logitech ConferenceCam CC3000e

### Je veux partager mon écran et/ou faire un appel audio :

1. Je me branche en USB-C sur la station d'accueil DELL (câble USB-C noir). 

{{% notice note %}}
Attention, une rallonge USB-C grise est présente afin de vous permettre de vous mettre en bout de table. Elle peut rendre la connexion avec certains PCs capricieuse. Si quelque chose ne marche pas, pensez à la retirer.
{{% /notice %}}

{{% notice info %}}
Il vaut mieux mettre son écran en recopie et orienter un écran de chaque côté de la table, afin que tout le monde voie bien
{{% /notice %}}

2. Je sélectionne la ConferenceCam CC3000e en entrée et en sortie audio

### Je veux faire une visioconférence :

1. Sur mon poste je lance le logiciel de visioconférence de mon choix
2. Au moment d'activer la caméra, je veille à bien sélectionner ConferenceCam CC3000e

