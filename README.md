# URL du site publié :

https://b5244.gitlab.io/epreuve-e4/documentation_batiment/

# Comment contribuer :

## Faire remonter des bugs, erreurs, desiderata, ... :

Faîtes tout simplement une issue sur ce projet

## Écrire de la documentation :

Pour des explications plus complètes, voir la documentation de hugo et du thème Learn que nous utilisons ici :

[Documentation de Hugo](https://gohugo.io/)

[Documentation du thème Learn](https://learn.netlify.app/en/)

### Structure de dossiers de Hugo :

https://learn.netlify.app/en/cont/pages/

``` 
doc_batiment
|--> content                 # C'est ici qu'on rédige le contenu du site
|    |--> un sujet
|    |    |--> _index.md
|    |    |--> page1.md
|    |    |--> page2.md
|    |    |--> ...
|    |--> un autre sujet
|    |    |--> _index.md
|    |    |--> ...
|    |--> _index.md
|--> layouts                 # Morceaux de HTML qui sont utilisés par le moteur de Hugo
|    |--> partials            # Pour customiser ou créer de nouveaux partials
|    |    |--> ...
|    |--> shortcodes          # Pour customiser ou créer de nouveaux shortcodes 
|         |--> ...
|--> ressources              # Dossier pour mettre des ressources, non utilisé
|    |--> ...
|--> static                  # Pour les ressources du site, utilisé
|    |--> css                 # Pour mettre le ... roulement de tambour ... CSS personnalisé du site
|    |    |--> style1.css
|    |    |--> style1000.css
|    |    |--> ...
|    |--> images              # Incroyable, vous ne devinerez jamais ce qu'on met là, créez un dossier par page du site rédigée, pour faciliter la lisibilité
|    |    |--> PEV_nu.jpg
|    |    |--> plan_cadavres_chape_beton.png
|    |    |--> ...
|    |--> ressources          # Pour les ressources diverses, pour l'instant les zip des polices d'écriture
|        |--> ...
|--> themes                  # C'est là qu'est l'arborescence des themes, PAS TOUCHE ! Les fichiers dans l'arborescence ci dessus overrident ceux du thème
|    |--> untheme
|    |    |--> ...
|    |--> unautretheme
          |--> ...
```

### Écrire une page :

Il faut créer le fichier Markdown à l'endroit voulu de l'arborescence des pages dans /content/ en utilisant ce modèle :

```markdown
+++
menuTitle = "Le titre à afficher dans le menu"   # OPTIONNEL : Titre à afficher dans le menu de gauche
pre = "<i class='fas fa-video'></i>"             # OPTIONNEL : Icône font awesome qui apparait avant le titre dans le menu
title = "Le titre de la page"
alwaysopen = true                                # OPTIONNEL : Seulement pour les pages _index.md des dossiers, indique que l'on souhaite que l'arborescence de ce dossier reste toujours ouverte dans le menu
+++

# BLABLATITRE

blablabla

du contenu

## Ho oui du contenu

```

### Mettre une photo (ou une autre ressource) :

Il faut déposer l'image dans le dossier /static/images/

Les liens dans le code markdown seront relatifs à /static/ donc de la forme : ```![une image sera ici](/images/ledossierdemapage/monimage.jpg)```
